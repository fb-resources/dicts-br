# Dictionaries

Phonetic, syllabic and stress vowel dictionaries.

Scripts can be found on GitHub: https://github.com/falabrasil/dicts-br


## License

MIT


## Citation

TBD (Eurasip 2022)


## Acknowledgements

- [ALMAnaCH research team](https://almanach.inria.fr/index-en.html) a
  [Inria](https://www.inria.fr/en) for the development of the
  [OSCAR corpus](https://oscar-corpus.com/), with a special props to
  [Pedro Ortiz](https://portizs.eu/).
- [FAPESPA](http://www.fapespa.pa.gov.br/) research funding for sponsoring a
  desktop machine with a minimal infrastructure to make it possible the
  training of such models


[![FalaBrasil](https://gitlab.com/falabrasil/avatars/-/raw/main/logo_fb_git_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](https://gitlab.com/falabrasil/avatars/-/raw/main/logo_ufpa_git_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2021)__ - https://ufpafalabrasil.gitlab.io/    
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Cassio Batista - https://cassota.gitlab.io/

